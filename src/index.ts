import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaixaAlertaModule } from './caixa-alerta/index';
export { CaixaAlertaModule } from './caixa-alerta/index';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
  ],
  exports: [
  ]
})
export class VieceliCore {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: VieceliCore,
      providers: []
    };
  }
}
