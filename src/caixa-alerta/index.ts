import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AlertModule} from 'ngx-bootstrap';
import {CaixaAlertaComponent} from './component/caixa-alerta.component';
import {GrowlModule} from 'primeng/components/growl/growl';
import {CaixaAlertaService} from './service/caixa-alerta.service';

@NgModule({
    imports     : [
        CommonModule,
        AlertModule.forRoot(),
        GrowlModule,
    ],
    exports     : [CaixaAlertaComponent],
    declarations: [CaixaAlertaComponent],
    providers: [CaixaAlertaService]
})
export class CaixaAlertaModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CaixaAlertaModule,
            providers: [CaixaAlertaService]
        };
    }
}
