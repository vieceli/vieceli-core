import { Injectable } from '@angular/core';

@Injectable()
export class CaixaAlertaService  {

    titulo      : string;
    icone       : string;
    mensagem    : string;
    tipo        : string;
}