import {Component, Input} from '@angular/core';
import {Message} from "primeng/components/common/api";

@Component({
    selector: 'core-caixa-alerta',
    templateUrl: './caixa-alerta.component.html',
})
export class CaixaAlertaComponent {
    private _tipo : string;
    private msg : Message[]  = [];
    private severity : string;

    @Input() titulo : string;
    @Input() icone : string;
    @Input() mensagem : string;
    @Input() semPadding : boolean = false;
    @Input() primeng: boolean = true;

    @Input() set tipo(valor : string){
        if(this.mensagem) {
            this.icone = valor;
            this._tipo = valor;
            this.severity = valor;
            switch (valor) {
                case 'warning':
                    this.titulo = 'Aviso!';
                    break;
                case 'success':
                    this.titulo = 'Sucesso!';
                    this.icone = 'fa-check';
                    break;
                case 'danger':
                    this.titulo = 'Erro!';
                    this.icone = 'fa-close';
                    this.severity = 'error';
                    break;
                default:
                case 'success':
                    this.titulo = 'Informação!';
                    break;
            }
            if(this.primeng) {
                this.msg.push({severity: this.severity, summary: this.titulo, detail: this.mensagem});
            }
        }
    }

    getStyle(){
        if(!this.semPadding){
            return {'padding-top': '78px'};
        }
    }

}